# README #

# Net4Care MHD FHIR Server

The MHD FHIR Server project develops a standalone component implementing the IHE *Mobile access to Health Documents* (MHD) profile. 
It is based on the version of the MHD profile that uses [HL7 FHIR (DSTU2)](https://www.hl7.org/fhir/).

**NOTICE:** 
This project is under development and the initial check ins only support:

 - Search on patient id's on DocumentReferences and DocumentManifests and almost no metadata filled in

 - Getting document via Binary resource

 
## Governance
The project is governed by 4S, with source control on [Bitbucket](https://bitbucket.org/4s/net4care-mhd-fhir-server)
and issue tracking in [JIRA](https://issuetracker4s.atlassian.net/browse/MHD).

## Develop

The project depends on the [Net4Care XDS connector](https://bitbucket.org/4s/net4care-xds-connector) component,
which it will fetch from the [4S Maven repository](http://artifactory.4s-online.dk), if it is not built and
installed locally.

 - Clone project from Bitbucket:  
   `git clone https://bitbucket.org/4s/net4care-mhd-fhir-server.git`

 - Compile:  
   `mvn install -DskipTests` from the root folder. 

 - Run the server:  
   Deploy built war file to a web container / servlet engine (tested with Apache Tomcat)

 - Test:  
   'mvn test' (Assumes that you have deployed the war file to a running web container / servlet engine)


##Licenses

The Net4Care MHD FHIR Server is under governance by [4S](http://www.4s-online.dk) and is licensed 
under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0).

The project depends on the 4S component [Net4Care XDS connector](https://bitbucket.org/4s/net4care-xds-connector),
also licensed under the Apache 2.0 license.

The project depends on the following third-party projects:

     (Apache Software License 2.0) HAPI FHIR - Core Library (ca.uhn.hapi.fhir:hapi-fhir-base:1.3 - http://jamesagnew.github.io/hapi-fhir/)
     (Apache Software License 2.0) HAPI FHIR Structures - DSTU2 (FHIR v1.0.0) (ca.uhn.hapi.fhir:hapi-fhir-structures-dstu2:1.3 - http://jamesagnew.github.io/hapi-fhir/hapi-deployable-pom/hapi-fhir-structures-dstu2/)
     (Apache Software License 2.0) HAPI FHIR TestPage Overlay (ca.uhn.hapi.fhir:hapi-fhir-testpage-overlay:1.3 - http://jamesagnew.github.io/hapi-fhir/hapi-fhir-testpage-overlay/)

     (Eclipse Public License - v 1.0) (GNU Lesser General Public License) Logback Classic Module (ch.qos.logback:logback-classic:1.1.2 - http://logback.qos.ch)

     (CDDL + GPLv2 with classpath exception) Java Servlet API (javax.servlet:javax.servlet-api:3.0.1 - http://servlet-spec.java.net)

     (The Apache Software License, Version 2.0) thymeleaf (org.thymeleaf:thymeleaf:2.1.4.RELEASE - http://www.thymeleaf.org)

     (The Apache Software License, Version 2.0) cors-filter (org.ebaysf.web:cors-filter:1.0.1 - https://github.com/ebay/cors-filter)

     (The Apache Software License, Version 2.0) Spring AOP (org.springframework:spring-aop:4.2.1.RELEASE - https://github.com/spring-projects/spring-framework)

     (The Apache Software License, Version 2.0) Spring Object/XML Marshalling (org.springframework:spring-oxm:4.2.1.RELEASE - https://github.com/spring-projects/spring-framework)

     (Apache License, Version 2.0) Spring Boot AutoConfigure (org.springframework.boot:spring-boot-autoconfigure:1.1.5.RELEASE - http://projects.spring.io/spring-boot/)