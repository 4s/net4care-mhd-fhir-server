package org.net4care.mhdfhirserver;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.resource.Binary;
import ca.uhn.fhir.model.dstu2.resource.DocumentManifest;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.ServerValidationModeEnum;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * To run the tests contained in TestClient you need to manually start a server instance of
 * the Net4Care mhd-fhir-server serving resources on http://localhost:8090/mhd-fhir-server/fhir/
 * Assumes the server is setup to run against XDS test servers at http://ihexds.nist.gov/
 */
public class TestClient {

    private static IGenericClient client;
    private static final FhirContext ourCtx = FhirContext.forDstu2();
    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(TestClient.class);

//    private static int ourPort;
//
//    private static Server ourServer;
    private static String ourServerBase;

    @Test
    public void testReadBinary() throws IOException {
        // Read NIST doc.entry with UniqueId 1.42.20141105202336.37
        String url = ourServerBase + "/Binary/1.42.20141105202336.37";

        Binary binary = client.read()
                .resource(Binary.class)
                .withUrl(url)
                .execute();

        String contentString = new String(binary.getContent());

        ourLog.info("Read Binary: " + "\n" + contentString);

        assertTrue(!contentString.isEmpty());
    }

    @Test
    public void testSearchDocumentReference() {
        // Search for DocumentReference's where Patient id = f10f8d972aba4fd
        // Corresponds to:
        // http://localhost:8090/mhd-fhir-server/fhir/DocumentReference?subject.identifier=f10f8d972aba4fd
        TokenClientParam patientId = new TokenClientParam(DocumentReference.SP_SUBJECT+'.'+Patient.SP_IDENTIFIER);
        ca.uhn.fhir.model.api.Bundle results = client
                .search()
                .forResource(DocumentReference.class)
                .where(patientId.exactly().code("f10f8d972aba4fd"))
                .execute();

        String resultString = ourCtx.newXmlParser().setPrettyPrint(true).encodeBundleToString(results);
        ourLog.info("Found DocumentReference's: " + "\n" + resultString);

        assertTrue(results.size() > 0);
    }

    @Test
    public void testSearchDocumentManifest() {
        // Search for DocumentManifest's where Patient id = f10f8d972aba4fd
        // Corresponds to:
        // http://localhost:8090/mhd-fhir-server/fhir/DocumentManifest?subject.identifier=f10f8d972aba4fd
        TokenClientParam patientId = new TokenClientParam(DocumentReference.SP_SUBJECT+'.'+Patient.SP_IDENTIFIER);
        ca.uhn.fhir.model.api.Bundle results = client
                .search()
                .forResource(DocumentManifest.class)
                .where(patientId.exactly().code("f10f8d972aba4fd"))
                .execute();

        String resultString = ourCtx.newXmlParser().setPrettyPrint(true).encodeBundleToString(results);
        ourLog.info("Found DocumentManifest's: " + "\n" + resultString);

        assertTrue(results.size() > 0);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        //ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        ourCtx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
        ourCtx.getRestfulClientFactory().setSocketTimeout(1200 * 1000);
        ourServerBase = "http://localhost:8090/mhd-fhir-server/fhir";
        client = ourCtx.newRestfulGenericClient(ourServerBase);
        client.registerInterceptor(new LoggingInterceptor(true));
    }

}
