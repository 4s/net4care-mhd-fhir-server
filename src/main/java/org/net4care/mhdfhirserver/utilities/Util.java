package org.net4care.mhdfhirserver.utilities;

import ca.uhn.fhir.rest.param.TokenParam;
import org.net4care.mhdfhirserver.servlet.MHDFHIRServlet;

public class Util {
    /**
     *
     * @param patientId Takes TokenParam as input where system part is patient ID assigning authority (e.g. "1.3.6.1.4.1.21367.2005.13.20.3000")
     *                  and value is the patient ID itself (f.ex: "f10f8d972aba4fd").
     *
     * @return Builds an XDS style patient identifier on the form identifier^^^&namespace&ISO.
     * If system part of input is null or empty default patient ID assigning authority will be read
     * in from properties.
     * Example output: "f10f8d972aba4fd^^^&1.3.6.1.4.1.21367.2005.13.20.3000&ISO"
     */
    public static String tokenParam2xdsPID (TokenParam patientId) {
        String patientIdAssigningAuth = patientId.getSystem();
        if (patientIdAssigningAuth == null || patientIdAssigningAuth.isEmpty())
            patientIdAssigningAuth = MHDFHIRServlet.xdsProperties.getProperty("xds.PIDdefaultAssigningAuth");
        return patientId.getValue() + "^^^&" + patientIdAssigningAuth + "&ISO";
    }

    /**
     *
     * @param uuid Takes UUID string as input
     * @return If the uuid entry parameter starts with "urn:uuid" it is stripped of and the bare UUID string is
     * returned.
     */
    public static String stripUrnUUID(String uuid) {
        String result = uuid;
        if (uuid != null && !uuid.isEmpty() && uuid.startsWith("urn:uuid:")) {
            result = uuid.substring(9);
        }
        return result;
    }
}
