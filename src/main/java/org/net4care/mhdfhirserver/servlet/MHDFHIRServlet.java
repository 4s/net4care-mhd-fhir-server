package org.net4care.mhdfhirserver.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.net4care.mhdfhirserver.provider.BinaryResourceProvider;
import org.net4care.mhdfhirserver.provider.DocumentManifestResourceProvider;
import org.net4care.mhdfhirserver.provider.DocumentReferenceResourceProvider;
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.narrative.DefaultThymeleafNarrativeGenerator;
import ca.uhn.fhir.narrative.INarrativeGenerator;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import org.net4care.xdsconnector.RegistryConnector;
import org.net4care.xdsconnector.RepositoryConnector;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

/**
 * This servlet is the actual FHIR server itself
 */
@EnableAutoConfiguration
public class MHDFHIRServlet extends RestfulServer {

    private static final long serialVersionUID = 1L;

    // Ideally, autowire these in IResourceProvider subclasses?
    // However, can't make Spring / HAPI play nice with this
    public static RegistryConnector xdsRegistry;
    public static RepositoryConnector xdsRepository;

    public static Properties xdsProperties;

    /**
     * Constructor
     */
    public MHDFHIRServlet() {
        super(FhirContext.forDstu2()); // Support DSTU2
    }

    /**
     * This method is called automatically when the
     * servlet is initializing.
     */
    @Override
    public void initialize() {

        // Get the spring context from the web container (it's declared in web.xml)
        WebApplicationContext parentAppCtx = ContextLoaderListener.getCurrentWebApplicationContext();
        ApplicationContext applicationContext = (ApplicationContext)parentAppCtx;

        //get XDS registry bean
        xdsRegistry = (RegistryConnector)applicationContext.getBean("xdsRegistryConnector");
        xdsRegistry.getDefaultUri();

        //get XDS repository bean
        xdsRepository = (RepositoryConnector)applicationContext.getBean("xdsRepositoryConnector");
        xdsRepository.getDefaultUri();
		/*
		 * Two resource providers are defined. Each one handles a specific
		 * type of resource.
		 */
        List<IResourceProvider> providers = new ArrayList<IResourceProvider>();
        providers.add(new DocumentReferenceResourceProvider());
        providers.add(new DocumentManifestResourceProvider());
        providers.add(new BinaryResourceProvider());
        setResourceProviders(providers);
		
		/*
		 * Use a narrative generator. This is a completely optional step, 
		 * but can be useful as it causes HAPI to generate narratives for
		 * resources which don't otherwise have one.
		 */
        INarrativeGenerator narrativeGen = new DefaultThymeleafNarrativeGenerator();
        getFhirContext().setNarrativeGenerator(narrativeGen);

		/*
		 * Tells HAPI to use content types which are not technically FHIR compliant when a browser is detected as the
		 * requesting client. This prevents browsers from trying to download resource responses instead of displaying them
		 * inline which can be handy for troubleshooting.
		 */
        setUseBrowserFriendlyContentTypes(true);

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        try {
            //Read in xds.properties:
            xdsProperties = new Properties();
            xdsProperties.load(classLoader.getResourceAsStream("xds.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
