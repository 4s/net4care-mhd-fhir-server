package org.net4care.mhdfhirserver.provider;

import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.DocumentManifest;
import ca.uhn.fhir.model.dstu2.resource.OperationOutcome;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.valueset.IssueSeverityEnum;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import org.net4care.mhdfhirserver.servlet.MHDFHIRServlet;
import org.net4care.mhdfhirserver.utilities.Util;
import org.net4care.xdsconnector.Constants.XDSStatusValues;
import org.net4care.xdsconnector.Utilities.FindSubmissionSetsQueryBuilder;
import org.net4care.xdsconnector.Utilities.GetAssociationsQueryBuilder;
import org.net4care.xdsconnector.Utilities.PrettyPrinter;
import org.net4care.xdsconnector.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import java.util.LinkedList;
import java.util.List;

import static org.net4care.mhdfhirserver.utilities.Util.tokenParam2xdsPID;

public class DocumentManifestResourceProvider implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(DocumentManifestResourceProvider.class);

    @Override
    public Class<? extends IResource> getResourceType() {
        return DocumentManifest.class;
    }

    /**
     * Search for Document Manifests.
     *
     * This implements the IHE ITI-66 Find Document Manifests transaction
     * for the MHD actor role Document Responder.
     *
     * The method will issue an IHE ITI-18 Registry Stored Query to the configured XDS Registry.
     * Specifically, it will issue a FindSubmissionSets query type followed by a
     * GetAssociations query type.
     *
     * @param patientId Specifies the patient identifier assigned to the DocumentManifest's searched for.
     *                  Format is "namespace|identifier". Example: 1.3.6.1.4.1.21367.2005.13.20.3000|f10f8d972aba4fd
     *                  Formats "identifier" and "|identifier" are also accepted.
     *                  Maps to ITI-18 $XDSSubmissionSetPatientId parameter.
     *                  Required parameter.
     *
     * @return This method returns a FHIR bundle of DocumentManifest resources matching
     * the query parameters. Based on ITI-18 queries returned DocumentManifest resources corresponding to SubmissionSet's
     * and the DocumentEntry's associated with the the SubmissionSet.
     *
     * Example: http://localhost:8090/mhd-fhir-server/fhir/DocumentManifest?subject.identifier=1.3.6.1.4.1.21367.2005.13.20.3000|f10f8d972aba4fd
     */
    @Search()
    public List<DocumentManifest>
    findDocumentManifests(@RequiredParam(name = DocumentManifest.SP_SUBJECT+'.'+ Patient.SP_IDENTIFIER) TokenParam patientId) {
        logger.debug("findDocumentManifests: Patient id assigning auth = " + patientId.getSystem() +
                " patient id value = " + patientId.getValue());

        LinkedList<DocumentManifest> retVal = new LinkedList<DocumentManifest>();

        String patientIdString = tokenParam2xdsPID(patientId);
        FindSubmissionSetsQueryBuilder findSubmissionSetsQueryBuilder = (FindSubmissionSetsQueryBuilder) new FindSubmissionSetsQueryBuilder()
                .setPatientId(patientIdString)
                .setSubmissionSetStatus(XDSStatusValues.DocumentEntry.Approved)
                .setReturnType(FindSubmissionSetsQueryBuilder.RETURN_TYPE_FULLMETADATA);
        AdhocQueryResponseType findSubmSetsQueryResponse = MHDFHIRServlet.xdsRegistry.executeQuery(findSubmissionSetsQueryBuilder);

        logger.debug(PrettyPrinter.prettyPrint(findSubmSetsQueryResponse));

        if (findSubmSetsQueryResponse.getStatus().equals(XDSStatusValues.RegistryResponse.Success)) {
            for(JAXBElement<? extends IdentifiableType> submSetElm : findSubmSetsQueryResponse.getRegistryObjectList().getIdentifiable()){
                IdentifiableType type = submSetElm.getValue();

                if (type instanceof RegistryPackageType) {
                    DocumentManifest documentManifest = new DocumentManifest();
                    RegistryPackageType registryPackage = (RegistryPackageType) type;

                    String xdsSubmissionSetUniqueID = "";
                    for (ExternalIdentifierType eit: registryPackage.getExternalIdentifier()) {
                        documentManifest.setId(Util.stripUrnUUID(registryPackage.getId()));
                        if (eit.getName().getLocalizedString().get(0).getValue().equals("XDSSubmissionSet.uniqueId")) {
                            IdentifierDt masterId = new IdentifierDt("urn:ietf:rfc:3986", "urn:oid:" + eit.getValue());
                            documentManifest.setMasterIdentifier(masterId);
                            break;
                        }
                    }

                    if (!xdsSubmissionSetUniqueID.isEmpty()) {
                        GetAssociationsQueryBuilder getassociationsQueryBuilder = (GetAssociationsQueryBuilder) new GetAssociationsQueryBuilder()
                                .setUUID(registryPackage.getId())
                                .setReturnType(GetAssociationsQueryBuilder.RETURN_TYPE_FULLMETADATA);
                        AdhocQueryResponseType getAssocQueryResponse = MHDFHIRServlet.xdsRegistry.executeQuery(getassociationsQueryBuilder);

                        if (getAssocQueryResponse.getStatus().equals(XDSStatusValues.RegistryResponse.Success)) {
                            for (JAXBElement<? extends IdentifiableType> assocElm : getAssocQueryResponse.getRegistryObjectList().getIdentifiable()) {
                                if (assocElm.getValue() instanceof AssociationType1) {
                                    AssociationType1 association = (AssociationType1)assocElm.getValue();
                                    String docEntryUUID = association.getTargetObject();
                                    DocumentManifest.Content content = documentManifest.addContent();
                                    content.setP(new ResourceReferenceDt("DocumentReference/"+docEntryUUID));
                                }
                            }
                        } else {
                            OperationOutcome oo = new OperationOutcome();
                            oo.addIssue().setSeverity(IssueSeverityEnum.ERROR).setDetails(PrettyPrinter.prettyPrint(getAssocQueryResponse));
                            throw new InternalErrorException("XDS GetAssociations query failed", oo);
                        }
                    }

                    retVal.add(documentManifest);
                }
            }
        } else {
            OperationOutcome oo = new OperationOutcome();
            oo.addIssue().setSeverity(IssueSeverityEnum.ERROR).setDetails(PrettyPrinter.prettyPrint(findSubmSetsQueryResponse));
            throw new InternalErrorException("XDS FindSubmissionSets query failed", oo);
        }

        return retVal;
    }

}
