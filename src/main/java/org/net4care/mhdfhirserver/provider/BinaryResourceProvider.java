package org.net4care.mhdfhirserver.provider;

import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.resource.Binary;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.server.IResourceProvider;
import org.junit.Assert;
import org.net4care.mhdfhirserver.servlet.MHDFHIRServlet;
import org.net4care.mhdfhirserver.utilities.Util;
import org.net4care.xdsconnector.Constants.XDSStatusValues;
import org.net4care.xdsconnector.Utilities.FindDocumentsQueryBuilder;
import org.net4care.xdsconnector.Utilities.GetDocumentsQueryBuilder;
import org.net4care.xdsconnector.Utilities.PrettyPrinter;
import org.net4care.xdsconnector.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;


public class BinaryResourceProvider implements IResourceProvider {
    private static final Logger logger = LoggerFactory.getLogger(BinaryResourceProvider.class);

    @Override
    public Class<? extends IResource> getResourceType() {
        return Binary.class;
    }

    /**
     * The "@Read" annotation indicates that this method supports the
     * read operation. Read operations should return a single resource
     * instance.
     *
     * @param theId
     *    The read operation takes one parameter, which must be of type
     *    IdDt and must be annotated with the "@Read.IdParam" annotation.
     * @return
     *    Returns a resource matching this identifier, or null if none exists.
     */
    @Read()
    public Binary getResourceById(@IdParam IdDt theId) {
        Binary binary = new Binary();

        logger.debug("getResourceById: theId.getIdPart = " + theId.getIdPart());

        //Use Doc. UniqueId to retrieve the actual document
        String docUniqueId = theId.getIdPart();
        if (!docUniqueId.isEmpty()) {
            RetrieveDocumentSetResponseType retrieveResponse = MHDFHIRServlet.xdsRepository.retrieveDocumentSet(docUniqueId);
            if (retrieveResponse.getRegistryResponse().getStatus().equals(XDSStatusValues.RegistryResponse.Success) &&
                    retrieveResponse.getDocumentResponse().size() > 0) {
                RetrieveDocumentSetResponseType.DocumentResponse documentResponse = retrieveResponse.getDocumentResponse().get(0);

                logger.debug("Retrieved document with MIMEtype = " + documentResponse.getMimeType());

                binary.setContentType(documentResponse.getMimeType());
                binary.setContent(documentResponse.getDocument()); //setContent takes care of base64 encoding
            } else {

                logger.error("Failed retrieving document with UniqueId = " + theId.getIdPart() +
                        " from XDS repository");
            }
        }

        return binary;
    }
}
