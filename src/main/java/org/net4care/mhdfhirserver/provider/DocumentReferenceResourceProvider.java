package org.net4care.mhdfhirserver.provider;


import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import org.net4care.mhdfhirserver.servlet.MHDFHIRServlet;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.rest.server.IResourceProvider;
import org.net4care.mhdfhirserver.utilities.Util;
import org.net4care.xdsconnector.Constants.XDSStatusValues;
import org.net4care.xdsconnector.Utilities.FindDocumentsQueryBuilder;
import org.net4care.xdsconnector.Utilities.PrettyPrinter;
import org.net4care.xdsconnector.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import java.util.LinkedList;
import java.util.List;

import static org.net4care.mhdfhirserver.utilities.Util.tokenParam2xdsPID;

public class DocumentReferenceResourceProvider implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(DocumentReferenceResourceProvider.class);

    @Override
    public Class<? extends IResource> getResourceType() {
        return DocumentReference.class;
    }

    /**
     * Search for Document References.
     *
     * This implements the IHE ITI-67 Find Document References transaction
     * for the MHD actor role Document Responder.
     *
     * The method will issue an IHE ITI-18 Registry Stored Query to the configured XDS Registry.
     * Specifically, it will issue a FindDocuments or FindDocumentsByReferenceId query.
     *
     * @param patientId Specifies the patient identifier assigned to the DocumentReference's searched for.
     *                  Format is "namespace|identifier". Example: 1.3.6.1.4.1.21367.2005.13.20.3000|f10f8d972aba4fd
     *                  Formats "identifier" and "|identifier" are also accepted.
     *                  Maps to ITI-18 $XDSDocumentEntryPatientId parameter.
     *                  Required parameter.
     *
     * @return This method returns a FHIR bundle of DocumentReference resources matching
     * the query parameters. Returned DocumentReference resources correspond to the DocumentEntry's
     * resulting from the IHE ITI-18 query.
     *
     * Example: http://localhost:8090/mhd-fhir-server/fhir/DocumentReference?subject.identifier=1.3.6.1.4.1.21367.2005.13.20.3000|f10f8d972aba4fd
     */
    @Search()
    public List<DocumentReference>
    findDocumentReferences(@RequiredParam(name = DocumentReference.SP_SUBJECT+'.'+Patient.SP_IDENTIFIER) TokenParam patientId) {
        logger.debug("findDocumentReferences: Patient id assigning auth = " + patientId.getSystem() +
                " patient id value = " + patientId.getValue());

        LinkedList<DocumentReference> retVal = new LinkedList<DocumentReference>();

        String patientIdString = tokenParam2xdsPID(patientId);

        FindDocumentsQueryBuilder findDocumentsQueryBuilder = (FindDocumentsQueryBuilder) new FindDocumentsQueryBuilder()
                .setPatientId(patientIdString)
                .addDocumentStatus(XDSStatusValues.DocumentEntry.Approved)
                .setReturnType(FindDocumentsQueryBuilder.RETURN_TYPE_FULLMETADATA);

        AdhocQueryResponseType queryResponse = MHDFHIRServlet.xdsRegistry.executeQuery(findDocumentsQueryBuilder);

        logger.debug(PrettyPrinter.prettyPrint(queryResponse));

        if (queryResponse.getStatus().equals(XDSStatusValues.RegistryResponse.Success)) {
            for(JAXBElement<? extends IdentifiableType> elm: queryResponse.getRegistryObjectList().getIdentifiable()){
                IdentifiableType type = elm.getValue();

                if (type instanceof ExtrinsicObjectType) {
                    DocumentReference docRef = new DocumentReference();

                    ExtrinsicObjectType extrinsicObject = (ExtrinsicObjectType) type;
                    //                    for (SlotType1 slot : extrinsicObject.getSlot()) {
                    //                        if ("creationTime".equals(slot.getName())) {
                    //                            dossier.setUpdated(slot.getValueList().getValue().get(0));
                    //                            break;
                    //                        }
                    //                    }

                    String xdsDocEntryUUID = "";
                    String xdsDocEntryUniqueId = "";
                    for (ExternalIdentifierType eit: extrinsicObject.getExternalIdentifier()) {
                        //FHIR logical ID is the entryUUID and is formatet without "urn:uuid" according to
                        // http://wiki.ihe.net/index.php?title=MHD-rev2-vol-3
                        xdsDocEntryUUID = Util.stripUrnUUID(extrinsicObject.getId());
                        docRef.setId(xdsDocEntryUUID);

                        if (eit.getName().getLocalizedString().get(0).getValue().equals("XDSDocumentEntry.uniqueId")) {
                            xdsDocEntryUniqueId = eit.getValue();
                            //Format masterIdentifier according to https://www.hl7.org/fhir/datatypes.html#Identifier
                            IdentifierDt masterId = new IdentifierDt("urn:ietf:rfc:3986", "urn:oid:" + xdsDocEntryUniqueId);
                            docRef.setMasterIdentifier(masterId);
                            break;
                        }
                    }

                    if (!xdsDocEntryUniqueId.isEmpty()) {
                        DocumentReference.Content docContent = docRef.addContent();
                        AttachmentDt contentAttachment = docContent.getAttachment();
                        //TODO: Possibility to configure if content is referenced or inline
                        //TODO: At least also set the required contentType and language on attachment
                        contentAttachment.setUrl("Binary/" + xdsDocEntryUniqueId);
                    }

                    retVal.add(docRef);
                }
            }

        }

        return retVal;
    }

    /**
     * The "@Read" annotation indicates that this method supports the
     * read operation. Read operations should return a single resource
     * instance.
     *
     * @param theId
     *    The read operation takes one parameter, which must be of type
     *    IdDt and must be annotated with the "@Read.IdParam" annotation.
     * @return
     *    Returns a resource matching this identifier, or null if none exists.
     */
    @Read()
    public DocumentReference getResourceById(@IdParam IdDt theId) {
        DocumentReference docRef = new DocumentReference();

        return docRef;
    }

}
